//const swc = require('semantic-crawler');
const Crawler = require('./');

const options = {
    wordlist: [
        ["contact", 30],
        ["company", 20],
        ["sales", 10],
        ["personnel", 35]
    ],
    worker: 4,
    max: 10000,
    timeout: 60, //seconds
    progress: function(response) {
        if (response) {
            info("time: %d link: %d", response.limits.time, response.limits.link);
        }
    }
};

Crawler('https://www.nanomagnetics-inst.com', options).then(function(result) {
    debugger;
});
