var Async = require('Async');
var CrawlerRequest = require('crawler-request');
var Debug = require('debug');
const PolarityRate = require('polarity-rate');
const URLParser = require('crawler-url-parser');

//for debugging purpose
Debug.log = console.log.bind(console);
var info = Debug('autokent:log');
var error = Debug('autokent:error');
info("package initialized.");

const DEFAULT_OPTIONS = {
    wordlist: [
        ["contact", 30]
    ],
    worker: 4,
    max: 10000,
    timeout: 10000,
    progress: function(response) {
        if (response) {
            info("time: %d link: %d", response.limits.time, response.limits.link);
        }
    }
};

function semantic_crawler(headUrl, options) {

    if (typeof headUrl == 'undefined') throw 'headUrl must be an valid url.';
    if (typeof options == 'undefined') options = DEFAULT_OPTIONS;
    if (typeof options.wordlist != 'object') options.wordlist = DEFAULT_OPTIONS.wordlist;
    if (typeof options.worker != 'number') options.worker = DEFAULT_OPTIONS.worker;
    if (typeof options.max != 'number') options.max = DEFAULT_OPTIONS.max;
    if (typeof options.timeout != 'number') options.timeout = DEFAULT_OPTIONS.timeout;
    if (typeof options.progress != 'function') options.progress = DEFAULT_OPTIONS.progress;

    let parsedUrl = URLParser.parse(headUrl);

    if (parsedUrl) headUrl = parsedUrl.url;
    else throw `invalid url ${headUrl}`;

    info("semantic crawler started.");

    return new Promise(function(resolve, reject) {
        let total_url = 0;
        let start = process.hrtime();
        let cacheMap = new Map();
        let cacheExternalMap = new Map();
        let limits = {
            link: 0,
            time: 0
        };

        let result = {
            total: 0,
            time: 0,
            crawled: [],
            external: [],
            errors: {
                timeout: [],
                notfound: [],
                forbidden: [],
                unsupported: [],
                pdf: [],
                crawler: []
            }
        };

        // create a queue object with concurrency `options.worker`
        let q = Async.priorityQueue(function(task, callback) {
            info("started:%o\n", task.queueItem);

            if (limits.link >= 100 || limits.time >= 100) {
                debugger;
                q.kill();
                resolve(result);
            }

            CrawlerRequest(task.queueItem.url)
                .then(function(response) {
                    // calculate site polarity
                    response.rate = 0;
                    if (response.type != "none" && response.text) {
                        result.crawled.push(response.url);
                        try {
                            let polarity = PolarityRate(response.text, options.wordlist);
                            response.rate = polarity.rate;
                        } catch (err) {
                            debugger;
                        }
                    } else if (response.error != "unsupported-extension") {
                        //debugger;
                    }

                    switch (response.status) {
                        case 200:
                            if (response.type == "none" || !response.text) {
                                debugger;
                                //log
                                //"http://www.fizik.itu.edu.tr/turhan/digital/dersicerigi_digital.electronics.doc"
                            }
                            break;
                        case 404:
                            result.errors.notfound.push(response.url);
                            break;
                        case 403:
                        case 401:
                            result.errors.forbidden.push(response.url);
                            break;
                        case 408:
                            result.errors.timeout.push(response.url);
                            break;
                        case -100:
                            result.errors.unsupported.push(response.url);
                            break;
                        case -111:
                            result.errors.crawler.push(response.url);
                            break;
                        case -222:
                            result.errors.pdf.push(response.url);
                            break;
                        default:
                            debugger;
                    }



                    return response;
                })
                .then(function(response) {
                    // extract links
                    response.links = [];

                    if (response.html != null) {
                        let links = URLParser.extract(response.html, response.url);
                        response.links = links ? links : [];
                    }

                    return response;
                })
                .then(function(response) {
                    //set link type,and rate algo(parent,current)
                    for (let urlObj of response.links) {

                        try {
                            urlObj.type = URLParser.gettype(urlObj.url, headUrl);
                        } catch (err) {
                            debugger;
                        }


                        if (task.queueItem.rate) {
                            urlObj.rate = PolarityRate.AddRate(response.rate, task.queueItem.rate);
                        } else {
                            urlObj.rate = response.rate;
                        }

                        let urlTextRate = PolarityRate(`${urlObj.url} ${urlObj.text}`, options.wordlist);
                        urlObj.rate = PolarityRate.AddRate(urlObj.rate, urlTextRate.rate);
                    }
                    return response;
                })
                .then(function(response) {
                    // set link type, and rate
                    for (let urlObj of response.links) {
                        urlObj.type = URLParser.gettype(urlObj.url, headUrl);

                        switch (urlObj.type) {
                            case "sublevel":
                                urlObj.rate = PolarityRate.AddRate(urlObj.rate, 6);
                                break;
                            case "samelevel":
                                urlObj.rate = PolarityRate.AddRate(urlObj.rate, 5);
                                break;
                            case "uplevel":
                                urlObj.rate = PolarityRate.AddRate(urlObj.rate, 4);
                                break;
                            case "internal":
                                urlObj.rate = PolarityRate.AddRate(urlObj.rate, 3);
                                break;
                            case "subdomain":
                                urlObj.rate = PolarityRate.AddRate(urlObj.rate, 2);
                                break;
                            case "updomain":
                                urlObj.rate = PolarityRate.AddRate(urlObj.rate, 1);
                                break;
                            case "external":
                                urlObj.rate = PolarityRate.AddRate(urlObj.rate, 0);
                                break;
                            default:
                                throw `invalid url type, ${urlObj.type}`;
                        }
                    }

                    return response;
                })
                .then(function(response) {
                    // update progress limits
                    let link_limit = (++total_url / options.max) * 100;
                    let time_limit = (process.hrtime(start)[0] / options.timeout) * 100;

                    result.time = process.hrtime(start)[0];

                    response.limits = {
                        link: link_limit,
                        time: time_limit
                    };

                    limits = response.limits;

                    response.queueItem = task.queueItem;

                    options.progress(response);

                    cacheMap.set(task.queueItem.url, task.queueItem);

                    return response;
                })
                .then(function(response) {
                    // push internal urls
                    for (let urlObj of response.links) {
                        switch (urlObj.type) {
                            case "samelevel":
                            case "sublevel":
                            case "uplevel":
                            case "internal":
                                if (!cacheMap.has(urlObj.url) && (response.limits.link <= 100 || response.limits.time <= 100)) {
                                    cacheMap.set(urlObj.url, {});
                                    q.push({
                                        queueItem: {
                                            url: urlObj.url,
                                            rate: urlObj.rate
                                        }
                                    }, urlObj.rate * -1);
                                }
                                break;
                            case "updomain":
                            case "subdomain":
                            case "external":
                                if (!cacheExternalMap.has(urlObj.url)) {
                                    cacheExternalMap.set(urlObj.url, urlObj);
                                } else {
                                    //avg rate

                                }
                                break;
                            default:
                                throw `invalid url type, ${urlObj.type}`;
                        }
                    }

                    callback();

                })
                .catch((err) => {
                    debugger;
                    let url_rate = total_url++/options.max;
                    //options.progress(url_rate);
                    callback(err);
                });

        }, options.worker);

        //push head url, priority:0
        q.push({
            queueItem: {
                url: headUrl,
                rate: null
            }
        }, 0);

        q.drain = function(err) {

            debugger;
            info('all items have been processed');
            resolve(result);
        };

        q.error = function(err) {
            debugger;
            error("error:%o", err);
        }



    });

}

module.exports = semantic_crawler;
