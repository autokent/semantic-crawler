# notes

//create a queue object with concurrency 2
var q = async.priorityQueue(function(task, callback) {
    console.log('hello ' + task.name);
    callback("aaa");
}, 2);

// assign a callback
q.drain = function() {
    console.log('all items have been processed');
};

// add some items to the queue
q.push({name: 'task-0'},0, function(res) {
    console.log('finished processing task-0:'+res);
});
q.push({name: 'task-100'},100, function (res) {
    console.log('finished processing task-100'+res);
});