# semantic-crawler
**Intelligent crawler, using text mining & polarity analysis for queue item priority.**

[![version](https://img.shields.io/npm/v/semantic-crawler.svg)](https://www.npmjs.org/package/semantic-crawler)
[![downloads](https://img.shields.io/npm/dt/semantic-crawler.svg)](https://www.npmjs.org/package/semantic-crawler)
[![node](https://img.shields.io/node/v/semantic-crawler.svg)](https://nodejs.org/)
[![status](https://gitlab.com/autokent/semantic-crawler/badges/master/pipeline.svg)](https://gitlab.com/autokent/semantic-crawler/pipelines)

## Installation
`npm install semantic-crawler`

## Usage

### Crawl
```js
const crawler = require('semantic-crawler');

crawler.crawl("http://science.sciencemag.org/").then(function(result){
    //console.log(result);
});

## Options

### Default Options


## Test
`mocha` or `npm test`

check [test folder](https://gitlab.com/autokent/semantic-crawler/tree/master/test) and [QUICKSTART.js](https://gitlab.com/autokent/semantic-crawler/blob/master/QUICKSTART.js) for extra usages.

## Support
I use this package actively myself, so it has my top priority.

### Submitting an Issue
If you find a bug or a mistake, you can help by submitting an issue to [GitLab Repository](https://gitlab.com/autokent/semantic-crawler/issues)

### Creating a Merge Request
GitLab calls it merge request instead of pull request.  

* [A Guide for First-Timers](https://about.gitlab.com/2016/06/16/fearless-contribution-a-guide-for-first-timers/)
* [How to create a merge request](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html)
* Check [Contributing Guide](https://gitlab.com/autokent/semantic-crawler/blob/master/CONTRIBUTING.md) 

### WhatsApp
Chat on WhatsApp about any infos, ideas and suggestions. 

[![WhatsApp](https://img.shields.io/badge/style-chat-green.svg?style=flat&label=whatsapp)](https://api.whatsapp.com/send?phone=905063042480&text=Hi%2C%0ALet%27s%20talk%20about%20semantic-crawler)

## License
[MIT licensed](https://gitlab.com/autokent/semantic-crawler/blob/master/LICENSE) and all it's dependencies are MIT or BSD licensed.